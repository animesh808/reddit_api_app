import React, {Component} from 'react';
import axios from 'axios';

class Apicall extends Component {

  componentWillMount(){
    this.getRedditData();
  }

  getRedditData(){
    axios.get(`https://www.reddit.com/r/${this.state.redditSub}.json`)
    .then(result => {
      console.log(result)
      const data = result.data.data.children.map(obj => obj.data)
      this.setState({redditData: data})
    })
    .catch(error => {
      this.setState({errorMsg: error.message})
    });
  }

  constructor(props){
    super(props);
    this.state = {
      redditData: [],
      redditSub: 'technology',
      errorMsg: ''
    };
    this.getRedditData = this.getRedditData.bind(this)
  }

  render(){
    return(
      <div>
        <h1>{`/r/${this.state.redditSub}`}</h1>
        <ul>
          {this.state.redditData.map(item =>
            <li key={item.id}>{item.title}</li>
          )}
        </ul>
      </div>
    );
  }
}

export default Apicall;
